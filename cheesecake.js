var http = require('http');
var accountSid = 'AC2d15ce4c702c3ebb1c13ae3a7c2c668d';
var authToken = '7c3fcc347541e397db06efcc1ef204eb';
var twilio = require('twilio')(accountSid, authToken);

var jeannieAuthToken = '61VwNgL8usmshPTJ2Av5sZDDgFMVp119mHEjsnEw1Bv4MT1B4A';
var unirest = require('unirest');

var input;

http.createServer(function (req, res) {

	var body;
	var twiml;

	req.on('data', function(data) {
		
		body += data;

	});
		
	req.on('end', function() {
		if (body) {
			var start = body.search("&Body=");
			var end = body.search("&FromCountry");
			var msg = body.substring(start+6,end);

			input = msg.replace(/\s+/g, '+');
			console.log('RECEIVED: ' + input);
			
			var response = jeannieRequest(input); 
	
			function jeannieRequest(input) {

				var getText;
				var response = unirest.get("https://jeannie.p.mashape.com/api?clientFeatures=all&input=" + input + "&locale=en&location=43.0%2C79.0&page=1&timeZone=%2B120")
				.header("X-Mashape-Key", "61VwNgL8usmshPTJ2Av5sZDDgFMVp119mHEjsnEw1Bv4MT1B4A")
				.end(function (result) {

					var myobj = JSON.parse(JSON.stringify(result.body));

					var getResult = myobj.output.forEach(function (eachElement) {
						if (eachElement.actions.say) {
							parsed = eachElement.actions.say.text.toString();
							getText = twimlWrapper(parsed, input);
							console.log('RESPONSE: ' + getText);
							res.writeHead(200, {'Content-Type': 'text/xml'});
							res.end(getText);
							return getText;
						} else {
							twiml = twimlWrapper('I do not understand.', input);
//							console.log('RESPONSE: I do not understand'); 
							res.writeHead(200, {'Content-Type': 'text/xml'});
							res.end(twiml);
						}
						});


				});
				getText = response;
			}

			} else {
				twiml = twimlWrapper('I do not understand.', input);
//				console.log('RESPONSE: I do not understand.');
				res.writeHead(200, {'Content-Type': 'text/xml'});
				res.end(twiml);
			}

	});
}).listen(9090);


function twimlWrapper(response, input) {

	console.log(input);

	if ((input.toLowerCase().search('cheesecake') === -1) || (input.toLowerCase().search('cheese+cake')) === -1)
	{
		var twiml = '<?xml version="1.0" encoding="UTF-8" ?><Response><message>'
		+ response.replace('Jeannie', 'Iva').replace('Master', 'Friend')
		+ '</message>'
		+ '</Response>';
	
	} else {

		var twiml = '<?xml version="1.0" encoding="UTF-8" ?><Response><MESSAGE>Yummy! How about an easy Oreo cheesecake?\n\nOnly 10 mins to prep and you\'ll need:\n\n2 pkg. (8oz. each) brick cream cheese\n\n1\\2 cup sugar\n\n1\\2 tsp. vanilla\n\n2 eggs\n\n6 Oreo cookies\n\n1 Oreo pie crust (6 oz.)\n\nLet me show you: http://bit.ly/1or7QjY</MESSAGE></Response>';

	}

	return twiml;
}

function jeannieRequest(input) {

	var getText;
	var response = unirest.get("https://jeannie.p.mashape.com/api?clientFeatures=all&input=" + input + "&locale=en&location=43.0%2C79.0&page=1&timeZone=%2B120")
	.header("X-Mashape-Key", "61VwNgL8usmshPTJ2Av5sZDDgFMVp119mHEjsnEw1Bv4MT1B4A")
	.end(function (result) {

		var myobj = JSON.parse(JSON.stringify(result.body));

		var getResult = myobj.output.forEach(function (eachElement) {
			parsed = eachElement.actions.say.text.toString();
			getText = twimlWrapper(parsed, input);
			console.log('function getText: ' + getText);
			return getText;
		});
				
	});
	getText = response;
	console.log('jeannieRequest outer getText: ' + getText);

}	
