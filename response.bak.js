var http = require('http');
var accountSid = 'AC2d15ce4c702c3ebb1c13ae3a7c2c668d';
var authToken = '7c3fcc347541e397db06efcc1ef204eb';
var twilio = require('twilio')(accountSid, authToken);
var sh = require('sh');
var jeannieAuthToken = '61VwNgL8usmshPTJ2Av5sZDDgFMVp119mHEjsnEw1Bv4MT1B4A';
var unirest = require('unirest');

http.createServer(function (req, res) {

		var body;

	    req.on('data', function(data) {
			    body += data;
		});
	
		req.on('end', function() {
			
			var start = body.search("&Body=");
			var end = body.search("&FromCountry");
			var msg = body.substring(start+6,end);

			var input = msg.replace(/\s+/g, '+');
			console.log(input);
			var response = jeannieRequest(input);

			var twiml = twimlWrapper(response);

//			console.log('body: ' + body);	
			console.log('twiml: ' + twiml);

			res.writeHead(200, {'Content-Type': 'text/xml'});
			res.end(twiml);
		});

}).listen(8080);


function twimlWrapper(response) {
			var twiml = '<?xml version="1.0" encoding="UTF-8" ?><Response><message>'
			+ 'You said: ' + response.toString()
			+ '</message>'
			+ '</Response>';
			return twiml;
}

function jeannieRequest(input) {
	var response = sh('curl --get --include "https://jeannie.p.mashape.com/text/?clientFeatures=all&input=' 
					+ input 
					+ '&locale=en&location=50.3%2C9.0&out=simple%2Fjson&timeZone=%2B120" -H "X-Mashape-Key: ' 
					+ jeannieAuthToken 
					+ '"').result(function (result) {
		
//						console.log('RESULT: ' + (result));
						
						return result;

			});

	console.log(response);	
	
//			var responseObj = JSON.parse(response);

//			var getResult = responseObj.output.forEach(function (eachElement) {
//				console.log(eachElement.actions.say.text);
//			});
	
	
	//	var responseObj = JSON.stringify(response);
	//('grep "text"')
	return response;	
}	
